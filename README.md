# Monitoreo y control de dispositivos USB mediante un API REST #

Este proyecto consiste en una aplicación para el **Monitoreo y control de dispositivos USB mediante un API REST**. La aplicación funciona como un daemon de Linux y consiste en dos procesos: **Monitor USB** y **Web Server**. Ambos procesos se comunican entre sí usando una interface de sockets. El proceso Monitor USB monitorea y lleva un registro de los dispositivos USB de almacenamiento masivo conectados al computador mientras que el proceso Web Server implementa un API REST que permite a usuarios remotos conocer e interactuar con los dispositivos USB conectados.

![picture](arquitectura.jpg)

## Autores ##
* Kevin Palacios
* Iván Aguirre

### Prerequisitos ###

#### Librerías externas ####

Para instalar estas librerías externas, en distribuciones basadas en Debian (Debian, Ubuntu, Raspbian, etc.), ejecute como root(sudo):

```
 apt-get install libudev1 libudev-dev libmicrohttpd-dev libjansson-dev libcurl4-gnutls-dev libgnutls28-dev libgcrypt20-dev
```


### Compilación ###

Este proyecto utiliza el Framework [ulfius](https://github.com/babelouest/ulfius/) creado por el usuario [babelouest](https://github.com/babelouest), el código de este Framework puede encontrarse en el directorio /lib de este proyecto. Por lo tanto la ejecución del archivo Makefile deberá hacerse con **permisos de administrador** para poder trabajar con el Framework.

**Compilar webserver y monitorusb:**
* ```sudo make```
	
### Uso ###
Ejecutar el MonitorUSB:

```
./monitorusb <port>
```
Ejemplo:

```
./monitorusb 8888
```

Ejecutar el WebServer :

```
./webserver <port> <monitorHost> <monitorPort> 
```
**Nota:** <monitorHost> , <monitorPort> son el host y puerto en los cuales se esta ejecutando el monitor usb

Ejemplo:

```
./webserver 8080 127.0.0.1 8888
```

# Documentación API REST #

###Listar Dispositivos###
----
  Retorna un json con la lista de dispositivos conectados en el servidor.

* **URL**

	`localhost:<port>/dispositivos`

* **Método:**

	`GET`
  
* **Parámetros de URL**
  
	`Ninguno`

* **Cuerpo Http**

	`Ninguno`

* **Ejemplo de llamada:**

	`http://localhost:8080/dispositivos`

* **Respuestas**

  	Código		|   Contenido
  	--------------- | ---------------
  	200 OK		| `{ "dispositivos": [ {"nombre": "...","id": "vendor:device","montaje": "/home/...","nodo": "/dev/..." }, { ...} ],"status": 0, "str_error" : ... }`
  	404 NOT FOUND	|`{ "dispositivos": [ ],"status": -1, "str_error" : ... }`
  	400 BAD REQUEST	|`No se ha encontrado el nodo '...'`
  	400 BAD REQUEST	|`El json no cumple la estructura`
  	500 INTERNAL SERVER ERROR|`No se ha podido establecer conexión con el monitor usb`





##Listar Archivos##
----
  Retorna un json con la lista de archivos del usb especificado.

* **URL**

	`localhost:<port>/archivos`

* **Método:**

	`POST`
  
*  **Parámetros de URL**
  
	`Ninguno`

* **Cuerpo Http**

	`{ "solicitud": "listar" , "nodo":"..." }`

* **Ejemplo de llamada:**

	`http://localhost:8080/archivos`
     
	Cuerpo: `{ "solicitud": "listar" , "nodo":"/dev/sdb" }`

* **Respuestas**

	Código		|   Contenido
	--------------- | ---------------
	200 OK		|`{"archivos": [{"nombre":"...", "size":"..."},{...} ], "status": 0, "str_error" : ... }`
	404 NOT FOUND	|`{"archivos": [], "status": -1, "str_error" : ... }`
	400 BAD REQUEST	|`No se ha encontrado el nodo '...'`
	400 BAD REQUEST	|`El json no cumple la estructura`
	500 INTERNAL SERVER ERROR|`No se ha podido establecer conexión con el monitor usb`



##Crear Archivos##
----
  Crea un archivo dentro de un dispositivo usb especificado.

* **URL**

	`localhost:<port>/archivos`

* **Método:**

	`POST`
  
*  **Parámetros de URL**
  
	`Ninguno`

* **Cuerpo Http**

	`{ "solicitud": "crear" , "nodo":"..." ,"nombre":"..." , "contenido":"..." }`

* **Ejemplo de llamada:**

	`http://localhost:8080/archivos`
    
    	Cuerpo: `{ "solicitud": "crear" , "nodo":"/dev/sdb", "nombre": "prueba.txt" , "contenido" : "Esto es una prueba de creación de archivos" }`


* **Respuestas**

  	Código				|   	Contenido
 	-------------------------------	| -------------------------------
 	 200 OK				|`Archivo creado exitosamente`
  	500 INTERNAL SERVER ERROR	|`error interno en el servidor`
  	400 BAD REQUEST			|`El json no cumple la estructura`
  	500 INTERNAL SERVER ERROR			|`No se ha podido establecer conexión con el monitor usb`


##Borrar Archivos##
----
  Borra un archivo dentro de un dispositivo usb especificado.

* **URL**

	`localhost:<port>/archivos`

* **Método:**

	`POST`
  
*  **Parámetros de URL**
  
	`Ninguno`

* **Cuerpo Http**

	`{ "solicitud": "borrar" , "nodo":"..." ,"nombre":"..."}`


* **Ejemplo de llamada:**

	`http://localhost:8080/archivos`
     
     	Cuerpo: `{ "solicitud": "borrar" , "nodo":"/dev/sdb" , "nombre" : "hola.txt" }`


  	Código		    |   Contenido
  	--------------- | ---------------
  	200 OK		    |`Archivo borrado exitosamente`
  	404 NOT FOUND	|`No existe el Archivo/Nodo enviado`
  	400 BAD REQUEST	|`No se ha encontrado el nodo '...'`
  	400 BAD REQUEST	|`El json no cumple la estructura`
  	500 INTERNAL SERVER ERROR|`No se ha podido establecer conexión con el monitor usb`


