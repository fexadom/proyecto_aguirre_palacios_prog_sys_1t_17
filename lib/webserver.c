#include "../include/webserver.h"
#include "../include/csapp.h"	

int monitor_conexion(char *hostname, char *port);
json_t * crear_json_error(int status , const char *str_error);
char * enviar_solicitud_monitor(json_t *solicitud);



char *monitor_host;
char *monitor_port;
sem_t semaforo;

/**
 * @POST
 * @Path(/archivos)
 * @Produces(application/JSON)
 * @Consumes(application/JSON)
 * obtiene la clave solicitud del json recibido y llama al metodo correspondiente
 */
int archivos_service(const struct _u_request * request, struct _u_response * response, void * user_data) {
	json_t * json_recibido = ulfius_get_json_body_request(request, NULL);
	json_t *solicitud;
	int solicitud_num;


	//verificando que sea un json object
	if(!is_object(json_recibido)){
		ulfius_set_string_body_response(response, BAD_REQUEST, "El json no cumple la estructura");
		return U_CALLBACK_CONTINUE;
	}
	//verifica que exista la clave solicitud
	if(!node_exists(json_recibido,"solicitud")){
		ulfius_set_string_body_response(response, BAD_REQUEST, "No se ha encontrado el nodo 'solicitud'");
		return U_CALLBACK_CONTINUE;
	}
	//obtiene el valor de la clave solicitud
	solicitud = json_object_get(json_recibido, "solicitud");
	solicitud_num = identificar_solicitud(solicitud);	

	//llama al metodo adecuado segun la solicitud
	switch (solicitud_num){
		case LISTAR:
			return listar_archivos(request,response);

		case BORRAR:
			return borrar_archivo(request,response);
		
		case CREAR:
			return crear_archivo(request,response);

	}
	

	return U_CALLBACK_CONTINUE;
}


/**
 * @GET
 * @Path(/archivos)
 * @Produces(application/JSON)
 * retorna un json con los nombres de los dispositivos conectados
 */
int listar_dispositivos(const struct _u_request * request, struct _u_response * response, void * user_data) {
		
	json_t * json_respuesta = NULL;
	char   *  str_respuesta;
	json_t * json_solicitud = json_object();          

	json_object_set_new( json_solicitud, "solicitud" , json_string("dispositivos"));

	
	//pedir al monitor los dispositivos conectados
	P(&semaforo);
	str_respuesta = enviar_solicitud_monitor(json_solicitud);
	V(&semaforo);
	json_error_t error;
	//convertir la respuesta del monitor en json_t
	json_respuesta =json_loads(str_respuesta,0,&error); 

	if(json_respuesta == NULL){ 
		json_respuesta = crear_json_error(ERROR, "No se ha podido establecer conexi�n con el monitor usb");
		ulfius_set_json_body_response(response, INTERNAL_SERVER_ERROR, json_respuesta);	
	}else{	
		ulfius_set_json_body_response(response, OK, json_respuesta);
		
	}
	json_decref(json_respuesta);
	json_decref(json_solicitud);
	return U_CALLBACK_CONTINUE;

}





/**
 * @POST
 * @Path(/archivos)
 * @Produces(application/JSON)
 * @Consumes(application/JSON)
 * retorna un json con los nombres de los archivos del nodo usb especificado en el json recibido con la etiqueta "nodo"
 */
int listar_archivos(const struct _u_request * request, struct _u_response * response) {
	json_t * json_recibido = ulfius_get_json_body_request(request, NULL);
	json_t * json_respuesta = NULL;
	char   *  str_respuesta;

	if(!node_exists(json_recibido,"nodo")) {		// si la clave nodo no existe en el json

		json_respuesta = crear_json_error(ERROR, "No se ha podido encontrar el nodo 'nodo'");
		ulfius_set_json_body_response(response, BAD_REQUEST, json_respuesta);

	}else{ 						// si todo esta bien se envia el json al monitor y se pide una respuesta
		
		str_respuesta = enviar_solicitud_monitor(json_recibido);
		json_error_t error;
		//convertir la respuesta del monitor en json_t
		json_respuesta =json_loads(str_respuesta,0,&error);
		if(json_respuesta == NULL){ 
			json_respuesta = crear_json_error(ERROR, "No se ha podido establecer conexi�n con el monitor usb");	
			ulfius_set_json_body_response(response, INTERNAL_SERVER_ERROR, json_respuesta);
		}else{
			int s =OK;
			json_t *status = json_object_get(json_recibido, "status");
			int status_int = json_integer_value(status);
			if(status_int <0)
				s=NOT_FOUND;
			ulfius_set_json_body_response(response, s, json_respuesta);
			json_decref(status);

		}
	}
	
	json_decref(json_respuesta);
	json_decref(json_recibido);
	return U_CALLBACK_CONTINUE;

}




/**
 * @POST
 * @Path(/archivos)
 * @Consumes(application/JSON)
 * borrar un archivo  de un dispositivo usb, el json recibido debe tener: nodo y nombre de archivo.
 */
int borrar_archivo(const struct _u_request * request, struct _u_response * response) {
	json_t * json_recibido = ulfius_get_json_body_request(request, NULL);
	char   *  str_respuesta;

	if(!node_exists(json_recibido,"nodo") ){
		ulfius_set_string_body_response(response, BAD_REQUEST, "No se ha podido encontrar el nodo 'nodo'");
		json_decref(json_recibido);
		return U_CALLBACK_CONTINUE;
	}
	if( !node_exists(json_recibido,"nombre")){
		ulfius_set_string_body_response(response, BAD_REQUEST, "No se ha podido encontrar el nodo 'nombre'");
		json_decref(json_recibido);
		return U_CALLBACK_CONTINUE;
	}
	str_respuesta = enviar_solicitud_monitor(json_recibido);

	
	if (strcmp(str_respuesta, "200") == 0)
		ulfius_set_string_body_response(response, OK, "Archivo borrado exitosamente");

	else if (strcmp(str_respuesta, "404") == 0)
		ulfius_set_string_body_response(response, NOT_FOUND, "No existe el Archivo/Nodo enviado");
	else
		ulfius_set_string_body_response(response, REQUEST_TIMEOUT, "No se pudo conectar con el monitor usb");
	
	json_decref(json_recibido);
	return U_CALLBACK_CONTINUE;


}


/**
 * @POST
 * @Path(/archivos)
 * @Consumes(application/JSON) 
 * crea un archivo en un dispositivo usb, el json recibio debe tener: nodo, nombre de archivo, contenido.
 */
int crear_archivo(const struct _u_request * request, struct _u_response * response) {
	json_t * json_recibido = ulfius_get_json_body_request(request, NULL);
	char   *  str_respuesta;
	
	if(!node_exists(json_recibido,"nodo")){
		ulfius_set_string_body_response(response, BAD_REQUEST, "No se ha podido encontrar el nodo 'nodo'");
		json_decref(json_recibido);
		return U_CALLBACK_CONTINUE;
	}
	if( !node_exists(json_recibido,"nombre") ){
		ulfius_set_string_body_response(response, BAD_REQUEST, "No se ha podido encontrar el nodo 'nombre'");
		json_decref(json_recibido);
		return U_CALLBACK_CONTINUE;
	}
	if(!node_exists(json_recibido,"contenido")   ){
		ulfius_set_string_body_response(response, BAD_REQUEST, "No se ha podido encontrar el nodo 'contenido'");
		json_decref(json_recibido);
		return U_CALLBACK_CONTINUE;
	}

	str_respuesta = enviar_solicitud_monitor(json_recibido);

	if (strcmp(str_respuesta, "200") == 0)
		ulfius_set_string_body_response(response, OK, "Archivo creado exitosamente");

	else if (strcmp(str_respuesta, "500") == 0)
		ulfius_set_string_body_response(response, INTERNAL_SERVER_ERROR, "Error interno en el servidor");

	else
		ulfius_set_string_body_response(response, REQUEST_TIMEOUT, "No se pudo conectar con el monitor usb");
	
	
	json_decref(json_recibido);
	return U_CALLBACK_CONTINUE;

}



int identificar_solicitud(json_t * solicitud){
	
	const char *solicitud_text;
	solicitud_text = json_string_value(solicitud);

	if (strcmp(solicitud_text, "listar") == 0) 
	{

	  return LISTAR;
	} 
	else if (strcmp(solicitud_text, "borrar") == 0)
	{
	  return BORRAR;
	}
	else if (strcmp(solicitud_text, "crear") == 0)
	{
	  return CREAR;
	}

	return U_CALLBACK_CONTINUE;

}

/** ----------------- MONITOR UTILS--------------- **/

int monitor_conexion(char *hostname, char *port){
	
	int clientfd;
	
	clientfd = open_clientfd(hostname, port);
	
	return clientfd;
	
}



char * enviar_solicitud_monitor(json_t *solicitud){
	int conn = monitor_conexion(monitor_host,monitor_port);
	if(conn <0){	
		return  "refused";

	}else{
		rio_t rio;
		char* buff = json_dumps(solicitud, 0);
		char respuesta_monitor[MAXLINE];
		Rio_readinitb(&rio, conn);
		strcat( buff, "\n");
		Rio_writen(conn, buff, strlen(buff));
		Rio_readlineb(&rio,respuesta_monitor, MAXLINE);
		char *respuesta = strtok(respuesta_monitor, "\n");
		Close(conn);
		return respuesta;

	}
}


/** ----------------- JSON UTILS--------------- **/

int is_object(json_t * json){
	if(!json_is_object(json)){
		
		syslog(LOG_ERR,"error: json recibido no es un objeto\n");
		json_decref(json);
		return 0;
	}
	return 1;

}

int node_exists(json_t * json, char * node){
	json_t * nodo;
	nodo = json_object_get(json, node);
	if(nodo == NULL){
		syslog(LOG_ERR,"error: json recibido no cumple la estructura\n");
		json_decref(json);
		return 0;
	}

	return 1;

}

json_t * crear_json_error(int status , const char *str_error){
	json_t * json_error = json_object();
	json_t *archivos = json_array();
	json_object_set_new( json_error, "archivos", archivos );
	json_object_set_new( json_error, "status", json_integer( status ));
	json_object_set_new( json_error, "str_error", json_string(str_error));
	return json_error;
}


void semaforo_init(){
	Sem_init(&semaforo, 0, 1);
} 
int test_monitor_conexion(char *monitorHost, char * monitorPort){
	monitor_host = monitorHost;
	monitor_port = monitorPort;
	int conn = monitor_conexion(monitor_host,monitor_port);
	if(conn <0)
		return -1;
	semaforo_init();	
	Close(conn);
	return 1;
}



