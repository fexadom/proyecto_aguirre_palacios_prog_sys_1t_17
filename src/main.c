#include "../include/webserver.h"
#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>


void daemonize(const char *cmd);


int main(int argc, char **argv)
{
	int val  =1;
	int port;	
	

	if (argc != 4) {
		fprintf(stderr, "uso: %s <webServerPort>  <monitorUsbHost>  <monitorUsbPort>\n", argv[0]);
		exit(0);
	}
	int test = test_monitor_conexion(argv[2] , argv[3]);
	if( test < 0 ){
		fprintf(stderr, "monitor usb no encontrado\n ");
		exit(0);
	}
	
	port = atoi(argv[1]);

	
	struct _u_instance instance;

	if (ulfius_init_instance(&instance, port, NULL, NULL) != U_OK) {
		fprintf(stderr, "No se pudo instanciar el servidor, cerrando");
		exit(0);
	}
  	daemonize(argv[0]);

	// peso maximo de archivos subidos
	instance.max_post_param_size = 16*1024;


	// declaracion de los metodos endpoint usando metodos de la  libreria ulfiud
	ulfius_add_endpoint_by_val(&instance, "GET", DISPOSITIVOS, NULL, 0, &listar_dispositivos, NULL);
	ulfius_add_endpoint_by_val(&instance, "POST", ARCHIVOS, NULL, 0, &archivos_service, NULL);
	



	// desplegar el servidor
	if (ulfius_start_framework(&instance) == U_OK) {
		syslog(LOG_INFO,"Servidor desplegado correctamente");

	} else {
		syslog(LOG_ERR,"No se puso desplegar el servidor\n");
	}
	
	int endpoint_numbers_fd = instance.nb_endpoints;
	
	while(val){
		fd_set fds;
		struct timeval tv;
		FD_ZERO(&fds);
		FD_SET(endpoint_numbers_fd, &fds);
		tv.tv_sec = 0;
		tv.tv_usec = 0;

		Select(endpoint_numbers_fd+1, &fds, NULL, NULL, &tv);
		if (FD_ISSET(endpoint_numbers_fd, &fds)){
			if(endpoint_numbers_fd == 0){
				syslog(LOG_INFO,"Todos los endpoints fueron removidos, cerrando el servidor");
				val = 0;
			}
			
		}
	}
	

	syslog(LOG_INFO,"Servidor terminado\n");
	ulfius_stop_framework(&instance);
	ulfius_clean_instance(&instance);

}



void daemonize(const char *cmd) {
	int	i, fd0, fd1, fd2;
	pid_t	pid;
	struct rlimit	rl;


	/*
	 * Clear file creation mask.
	 */
	umask(0);

	/*
	 * Get maximum number of file descriptors.
	 */
	if (getrlimit(RLIMIT_NOFILE, &rl) < 0)
		unix_error("can't get file limit");

	/*
	 * Become a session leader to lose controlling TTY.
	 */
	if ((pid = fork()) < 0)
		unix_error("can't fork");
	else if (pid != 0) /* parent */
		exit(0);
	setsid();



	/*
	 * Change the current working directory to the root so
	 * we won't prevent file systems from being unmounted.
	 */
	if (chdir("/") < 0)
		unix_error("can't change directory to /");

	/*
	 * Close all open file descriptors.
	 */
	if (rl.rlim_max == RLIM_INFINITY)
		rl.rlim_max = 1024;
	for (i = 0; i < rl.rlim_max; i++)
		close(i);

	/*
	 * Attach file descriptors 0, 1, and 2 to /dev/null.
	 */
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(0);
	fd2 = dup(0);

	/*
	 * Initialize the log file.
	 */
	openlog(cmd, LOG_CONS, LOG_DAEMON);
	if (fd0 != 0 || fd1 != 1 || fd2 != 2) {
		syslog(LOG_ERR, "unexpected file descriptors %d %d %d",
		  fd0, fd1, fd2);
		exit(1);
	}
}




