CC = gcc
CFLAGS = -O2 -w -I .
LIBULFIUS_LOCATION=./lib/ulfius/src
LIBYDER_LOCATION=./lib/ulfius/lib/yder
LIBORCANIA_LOCATION=./lib/ulfius/lib/orcania

LLIBS = -ljansson -lulfius  -lpthread
MLIBS = -lpthread -ludev -ljansson 

all:  orcaniainstall yderinstall ulfiusinstall server monitor

server: src/main.c webserver.o csapp.o
	$(CC) $(CFLAGS) -o webserver $^ $(LLIBS)

monitor: lib/monitor_usb.c csapp.o
	$(CC) $(CFLAGS) -o monitorusb $^ $(MLIBS)
 
webserver.o: lib/webserver.c  include/webserver.h 
	$(CC) $(CFLAGS) -c $^

csapp.o: lib/csapp.c include/csapp.h
	$(CC) $(CFLAGS) -c $^ 

ulfiusinstall:
	cd $(LIBULFIUS_LOCATION) && $(MAKE) install
yderinstall:
	cd $(LIBYDER_LOCATION) && $(MAKE) install
orcaniainstall:
	cd $(LIBORCANIA_LOCATION) && $(MAKE) install
clean: 
	rm -f *.o
	rm -f *.a
	rm -f webserver
